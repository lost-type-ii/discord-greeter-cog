from PIL   import  Image, ImageFilter, ImageDraw,ImageFont
import numpy as np
import pathlib

def recursively_resolve_path(path:str) -> str:
    cwd = pathlib.Path().cwd()
    return str(next(cwd.glob("**/" + str(path))))

class ArrivalAnnouncementBuilder:
    def __init__(self):
        self.base_width = 960
        self.base_height = 437

        base_path          = recursively_resolve_path("assets/img/base.png")
        self.base = Image.open(base_path).convert("RGBA").resize((self.base_width, self.base_height))


        font_path_title    = recursively_resolve_path("assets/fonts/biosans-semibold.otf")
        font_path_subtitle = recursively_resolve_path("assets/fonts/biosans-light.otf")

        self.font_base = ImageFont.truetype(font_path_title, 28)
        self.font_support = ImageFont.truetype(font_path_subtitle, 24)


    def pfp(self, image:Image,
                  name:str = "A stranger",
                  number:int = 0):

        message1 = f"{name} just joined the server"
        message2 = f"Member #{number}"


        base_copy = self.base.copy()
        crop_image = self._tranparent_circular_crop(image)

        x, y = (377, 62)
        area = (x, y, x + 208, y + 208)
        base_copy.paste(crop_image, area, mask=crop_image)


        draw = ImageDraw.Draw(base_copy)
        w, h = draw.textsize(message1, font=self.font_base)
        draw.text(((self.base_width - w) / 2, 305), message1, fill=(250, 250, 250), font=self.font_base)

        w, h = draw.textsize(message2, font=self.font_support)
        draw.text(((self.base_width - w) / 2, 348), message2, fill=(154, 154, 154), font=self.font_support)


        return base_copy

    def _tranparent_circular_crop(self,img):
        h, w = img.size

        alpha = Image.new('L', img.size, 0)
        draw = ImageDraw.Draw(alpha)
        draw.pieslice([0, 0, h, w], 0, 360, fill=255)

        npImage = np.array(img)

        npAlpha=np.array(alpha)

        npImage=np.dstack((npImage,npAlpha))

        im = Image.fromarray(npImage).resize((208, 208))
        return im

