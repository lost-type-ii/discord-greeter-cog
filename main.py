from cog.arrival_announcement_builder import ArrivalAnnouncementBuilder
from PIL import Image

#### #### #### Open PFP #### #### #### #### #### ####
test_pfp = Image.open("arrival_announcement_builder/assets/test/test_pfp.png").convert("RGB")


#### #### #### Create Builder #### #### #### #### #### ####
builder = ArrivalAnnouncementBuilder()


#### #### #### Apply On User #### #### #### #### #### ####
banner:Image = builder.pfp(test_pfp, name = "Shinobu#1234", number= 15)

#show
banner.show()
banner.save("example_banner.png")
