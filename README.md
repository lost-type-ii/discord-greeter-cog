# Arrival Announcement Cog 

This cog generates a banner for new users joining a server and posts this banner in a designated arrival channel. 

## Example of an Arrival Banner

![](example_banner.png)