import discord
from discord.ext import commands
from .arrival_announcement_builder import ArrivalAnnouncementBuilder
from PIL import Image
import requests
from io import BytesIO

def load_image_from_discord_url(url:str) -> Image:
    response = requests.get(url)
    return Image.open(BytesIO(response.content))

def file_from_image(img:Image, file_name = "welcome") -> discord.File:
    final_buffer = BytesIO()
    img.save(final_buffer, "png")
    final_buffer.seek(0)
    file = discord.File(filename=f"{file_name}.png", fp=final_buffer)
    return file

class ArrivalBannerCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None
        self.banner_builder = ArrivalAnnouncementBuilder()
        print("Arrival Banner Cog was Generated")


    @commands.Cog.listener()
    async def on_member_join(self, member):
        channel = member.guild.system_channel

        if channel is not None:
            pfp_image:Image = load_image_from_discord_url(url= member.avatar_url)


            banner:Image = self.banner_builder.pfp(image = pfp_image,
                                                   name  = member.name,
                                                   number = len(member.guild.members))

            banner_file = file_from_image(banner)

            SOME_MESSAGE = ""

            await channel.send(SOME_MESSAGE, file = banner_file)

if __name__ == "__main__":

    bot = commands.Bot(command_prefix="")
    bot.add_cog(ArrivalBannerCog(bot))
    bot.run("")